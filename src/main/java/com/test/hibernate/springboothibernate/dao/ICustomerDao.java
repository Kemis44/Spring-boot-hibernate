package com.test.hibernate.springboothibernate.dao;

import com.test.hibernate.springboothibernate.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
@Transactional
public interface ICustomerDao extends CrudRepository<Customer, Integer> {
}
