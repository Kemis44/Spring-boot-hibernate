package com.test.hibernate.springboothibernate.dao;

import com.test.hibernate.springboothibernate.model.Customer;
import com.test.hibernate.springboothibernate.repository.ICustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerDao {

    private final ICustomerRepository customerRepository;

    @Autowired
    public CustomerDao(ICustomerRepository custRepo) {
        this.customerRepository = custRepo;
    }


    public Customer save(Customer customer){
        return customerRepository.save(customer);
    }

    public Iterable<Customer> findAll(){
        return customerRepository.findAll();
    }

    public Customer findById(int id){
        Optional<Customer> customer = customerRepository.findById(id);
        Customer cust = new Customer();

        if (customer.isPresent()){
            cust =  customer.get();
        }
        return cust;
    }

    public void delete(int id){
        Customer deletedCustomer = findById(id);

        customerRepository.delete(deletedCustomer);
    }

}
