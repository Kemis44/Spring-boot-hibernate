package com.test.hibernate.springboothibernate.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Getter
@Setter
@Table(name = "test")
public class Customer {

    @Id
    @GeneratedValue
    @Column(name = "uid", unique = true)
    @NotNull
    private int uid;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "age")
    private int age;


    public Customer(@NotNull int uid, String name, String surname, int age) {
        this.uid = uid;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public Customer() {
    }

}
