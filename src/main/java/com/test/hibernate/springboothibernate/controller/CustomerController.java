package com.test.hibernate.springboothibernate.controller;

import com.test.hibernate.springboothibernate.dao.ICustomerDao;
import com.test.hibernate.springboothibernate.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;



@Controller
@RequestMapping(value = "/api/customers")
public class CustomerController {

    private final ICustomerDao customerDao;

    @Autowired
    public CustomerController(ICustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @PostMapping(value = "/add")
    public String addCustomerToDb(@ModelAttribute Customer customer, Model model) {

        customerDao.save(customer);

        model.addAttribute("customers", customerDao.findAll());

        String url = "http://localhost:8080/api/customers/all";

        return "redirect:" + url;
    }

    @GetMapping(value = "/getAll")
    @ResponseBody
    public Iterable<Customer> getCustomers() {

        return customerDao.findAll();
    }

    @GetMapping(value = "/all")
    public String getAllCustomersView(Model model) {

        model.addAttribute("customers", customerDao.findAll());

        return "index";
    }

    @GetMapping(value = "/delete/{id}")
    public String deleteCustomer(@PathVariable int id, Model model) {

        customerDao.deleteById(id);

        String url = "http://localhost:8080/api/customers/all";

        model.addAttribute("customers", customerDao.findAll());

        return "redirect:" + url;
    }

    @GetMapping(value = "/add")
    public String addCustomerPage() {

        return "addCustomer";
    }


}
