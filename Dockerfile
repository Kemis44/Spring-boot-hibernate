FROM openjdk:8
VOLUME /tmp
ADD target/spring-boot-hibernate.jar spring-boot-hibernate.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "spring-boot-hibernate.jar"]